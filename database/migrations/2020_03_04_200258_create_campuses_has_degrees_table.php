<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampusesHasDegreesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campuses_has_degrees', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->boolean('status')->default(TRUE);

            // llaves foraneas de la tabla campuses_has_degrees
            $table->foreignId('degree_id')->constrained();
            $table->foreignId('campus_id')->constrained();
            $table->foreignId('user_id')->constrained()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campuses_has_degrees');
    }
}
