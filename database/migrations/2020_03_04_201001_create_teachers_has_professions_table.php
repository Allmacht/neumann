<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeachersHasProfessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teachers_has_professions', function (Blueprint $table) {
            $table->id();
            $table->timestamps();


            // llaves foraneas de la tabla teachers_has_professions
            $table->foreignId('personal_information_id')->constrained();
            $table->foreignId('profession_id')->constrained();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teachers_has_professions');
    }
}
