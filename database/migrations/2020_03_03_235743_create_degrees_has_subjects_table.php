<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDegreesHasSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('degrees_has_subjects', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('semester');

            // llaves foraneas de la tabla degrees_has_subjects
            $table->foreignId('degree_id')->constrained();

            $table->foreignId('subject_id')->constrained();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('degrees_has_subjects');
    }
}
