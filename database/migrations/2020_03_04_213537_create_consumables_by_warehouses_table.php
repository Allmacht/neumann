<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConsumablesByWarehousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consumables_by_warehouses', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->float('stock');
            $table->float('min_quantity');
            $table->boolean('status')->default(TRUE);


            // llave foranea de consumable
            $table->foreignId('areas_by_warehouse_id')->constrained();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consumables_by_warehouses');
    }
}
