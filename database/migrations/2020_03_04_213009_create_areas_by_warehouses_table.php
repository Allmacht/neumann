<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAreasByWarehousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('areas_by_warehouses', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('description');
            $table->boolean('status')->default(TRUE);
            $table->string('code')->nullable();

            // llaves foraneas de areas_by_warehouses
            $table->foreignId('warehouse_id')->constrained();
            $table->foreignId('area_id')->constrained();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('areas_by_warehouses');
    }
}
