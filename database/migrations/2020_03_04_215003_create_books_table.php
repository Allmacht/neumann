<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('name')->nullable();
            $table->string('original_name');
            $table->string('unique_code')->unique;
            $table->boolean('status')->default(TRUE);
            $table->integer('year')->nullable();
            $table->integer('number_sheets');
            
            // llaves foraneas de la tabla books
            $table->foreignId('author_id')->constrained();
            $table->foreignId('editorial_id')->constrained();
            $table->foreignId('gender_id')->constrained();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
