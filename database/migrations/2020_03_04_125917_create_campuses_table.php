<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campuses', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('code');
            $table->string('name');
            $table->string('state');
            $table->string('municipality');
            $table->string('colony')->nullable();
            $table->string('street');
            $table->string('zipcode');
            $table->string('external_number')->nullable();
            $table->string('internal_number')->nullable();
            $table->boolean('status')->default(TRUE);
            $table->string('level');
            $table->string('shift');
            $table->string('type');

            $table->foreignId('user_id')->constrained();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campuses');
    }
}
