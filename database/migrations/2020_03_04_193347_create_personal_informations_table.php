<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonalInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal_informations', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('gender');
            $table->date('birthdate');
            $table->string('CURP')->nullable();
            $table->string('municipality');
            $table->string('colony')->nullable();
            $table->string('street');
            $table->string('external_number')->nullable();
            $table->string('internal_number')->nullable();
            $table->string('zipcode');
            $table->string('phone_number');
            $table->string('contact_name');
            $table->string('contact_phone');
            $table->string('alergy_description')->nullable();
            $table->string('controlled_medication')->nullable();
            $table->boolean('status')->default(TRUE);
            $table->date('admission_date');
            $table->string('RFC')->nullable();
            $table->string('social_insurance')->nullable();
            $table->string('civil_status')->nullable();

            // llaves foraneas de la tabla personal_informations
            $table->foreignId('user_id')->constrained()->nullable();
            $table->foreignId('degrees_id')->constrained()->nullable();
            $table->foreignId('modality_id')->constrained();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal_informations');
    }
}
