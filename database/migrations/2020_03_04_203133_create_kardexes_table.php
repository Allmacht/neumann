<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKardexesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kardexes', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->boolean('status')->default(TRUE);
            $table->boolean('ordinary')->default(FALSE);
            $table->boolean('extra')->default(FALSE);
            $table->boolean('title')->default(FALSE);
            $table->boolean('repetition')->default(FALSE);
            $table->float('u1');
            $table->float('u2');
            $table->float('u3');
            $table->float('u4');
            $table->float('u5')->nullable();
            $table->float('average');
            

            // llave foranea de la tabla kardexes
            $table->foreignId('students_has_subject_id')->constrained();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kardexes');
    }
}
