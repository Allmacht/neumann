<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name             = 'SuperAdmin';
        $user->names            = 'SuperAdmin';
        $user->paternal_surname = 'SuperAdmin';
        $user->maternal_surname = 'SuperAdmin';
        $user->email            = 'neumann@neumann.com';
        $user->password         = bcrypt('admin');
        $user->save();

        $user->assignRole('super-admin');
    }
}
