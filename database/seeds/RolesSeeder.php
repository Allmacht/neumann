<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $super_admin   = Role::create(['name' => 'super-admin']);
        $Administrador = Role::create(['name' => 'Administrador' ]);
        $Coordinador   = Role::create(['name' => 'Coordinador']);
        $Bibliotecario = Role::create(['name' => 'Bibliotecario']);
        $Docente       = Role::create(['name' => 'Docente']);
        $Alumno        = Role::create(['name' => 'Alumno']);
      }
}
