<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use App\Mail\VerifyEmail;
use App\Mail\PasswordReset;
use App\Mail\PasswordResetNotification;
use Carbon\Carbon;
use App\User;
use App\Campus;

class UserController extends Controller
{
    public function index(Request $request)
    {

    }

    public function getUser(Request $request)
    {
        $roles = Auth::user()->getRoleNames();

        $user = array(
            'name' => Auth::user()->name,
            'email' => Auth::user()->email,
            'image' => 'user.png',
            'roles' => $roles
        );

        return response()->json(['user' => $user]);
    }

    public function guest_store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'user.names'                 => 'required',
            'user.paternal_surname'      => 'required',
            'user.maternal_surname'      => 'required',
            'user.name'                  => 'required',
            'user.email'                 => 'required|email|unique:users,email',
            'user.password'              => 'required|min:8',
            'user.password_confirmation' => 'required|min:8|same:user.password',
            'user.campus_code'           => 'required|exists:campuses,code',
            'user.role'                  => 'required'
        ],[
            'user.names.required'                 => 'Ingrese su nombre completo',
            'user.paternal_surname.required'      => 'El apellido paterno es requerido',
            'user.maternal_surname.required'      => 'El apellido materno es requerido',
            'user.name.required'                  => 'El nombre de usuario es requerido',
            'user.email.required'                 => 'El correo electrónico es requerido',
            'user.email.email'                    => 'El correo electrónico ingresado no es válido',
            'user.email.unique'                   => 'El correo electrónico ingresado ya está en uso',
            'user.password.required'              => 'La contraseña es requerida',
            'user.password.min'                   => 'La contraseña debe contener almenos 8 caracteres',
            'user.password_confirmation.required' => 'La confirmación de contraseña es requerida',
            'user.password_confirmation.min'      => 'La confirmación de contraseña debe contener almenos 8 caracteres',
            'user.password_confirmation.same'     => 'La confirmación de contraseña no coincide',
            'user.campus_code.required'           => 'El codígo de plantel es requerido',
            'user.campus_code.exists'             => 'El código del plantel no es válido',
            'user.role.required'                  => 'El rol de usuario es requerido'
        ]);

        if($validator->fails())
            return response()->json(['errors' => $validator->errors()],200);

        $user = new User();
        $user->names            = $request->user['names'];
        $user->paternal_surname = $request->user['paternal_surname'];
        $user->maternal_surname = $request->user['maternal_surname'];
        $user->name             = $request->user['name'];
        $user->email            = $request->user['email'];
        $user->password         = bcrypt($request->user['password']);
        $user->campus_id        = Campus::whereCode($request->user['campus_code'])->first()->value('id');

        $user->save();

        $user->assignRole($request->user['role']);

        if(Auth::attempt(['email' => $request->user['email'], 'password' => $request->user['password']])):
            $data = new \stdClass();
            @$data->user_name = Auth::user()->names;
            $data->email = Auth::user()->email;
            $data->link  = URL::signedRoute('email.validate', ['user' => Auth::user()->id]);
            try {
                $send = Mail::to($data->email)->send(new VerifyEmail($data));
                return response()->json(['success' => true], 200);

            } catch (\Exception $e) {
                return response()->json(['success' => true], 200);
            }
        endif;

    }

    public function resend_email(Request $request)
    {
        if($request->ajax()):
            $data = new \stdClass();
            @$data->user_name = Auth::user()->names;
            $data->email = Auth::user()->email;
            $data->link  = URL::signedRoute('email.validate', ['user' => Auth::user()->id]);

            try {
                $send = Mail::to($data->email)->send(new VerifyEmail($data));
                return response()->json(['success' => true], 200);

            } catch (\Exception $e) {
                return response()->json(['success' => false], 200);
            }

        endif;
    }

    public function validate_email($user, Request $request)
    {
        if($request->hasValidSignature()):
            $user = User::findOrfail($user);
            $user->email_verified_at = $current_date_time = Carbon::now()->toDateTimeString();
            $user->save();
            return redirect()->route('home');
        else:
            abort(404);
        endif;
    }

    public function email_reset(Request $request)
    {
        $user = User::whereEmail($request->email)->first();
        if(!is_null($user)):

            $old_signatures = DB::table('password_resets')->where('email',$user->email)->delete();

            DB::table('password_resets')->insert([
                'email' => $user->email,
                'token' => Str::random(60),
                'created_at' => Carbon::now()
            ]);

            $data = new \stdClass();
            @$data->user_name = $user->names;
            $data->email      = $user->email;
            $data->link       = URL::temporarySignedRoute('password.reset', now()->addMinutes(30) ,['user' => $user->id]);

            try{
                $send = Mail::to($data->email)->send(new PasswordReset($data));
                return response(['success' => true, 'user' => $data], 200);
            }catch(\Exception $e){
                return response(['success' => false, 'error' => $e], 200);
            }

        endif;

        return response(['success' => true, 'error' => true], 200);
    }

    public function password_reset(User $user, Request $request)
    {
        if($request->hasValidSignature()):
            $token = DB::table('password_resets')->where('email',$user->email)->value('token');
            return view('auth.passwords.reset', compact('token'));
        else:
            $old_signatures = DB::table('password_resets')->where('email',$user->email)->delete();
            abort(404);
        endif;
    }

    public function password_update(Request $request)
    {
        $validate = DB::table('password_resets')->where('token',$request->token)->where('email',$request->email)->get();
        if($validate->isEmpty()):
            return response()->json(
                [
                    'success' => false,
                    'errors' => array(
                        'email' => array(
                            "El correo electónico ingresado no es válido."
                        )
                    )
                ], 200);
        endif;

        $validator = Validator::make($request->all(),[
            'email'    => 'required|email|exists:users,email',
            'password' => 'required|min:8',
            'token'    => 'required|exists:password_resets,token'
        ],[
            'email.required'    => 'El correo electónico es requerido.',
            'email.email'       => 'El correo electónico no es válido.',
            'email.exists'      => 'El correo electónico no está asociado a ningúna cuenta.',
            'password.required' => 'la contraseña es requerida.',
            'password.min'      => 'La contraseña debe contener almenos 8 caracteres.',
            'token.required'    => 'Ha ocurrido un error en la validación de su sesión.',
            'token.exists'      => 'Ha ocurrido un error en la validación de su sesión.'
        ]);

        if($validator->fails())
            return response()->json(['success' => false, 'errors' => $validator->errors()], 200);

        $user = User::whereEmail($request->email)->first();
        $user->password = bcrypt($request->password);

        $user->update();
        $old_signatures = DB::table('password_resets')->where('email',$user->email)->delete();

        $data = new \stdClass();
        @$data->user_name = $user->names;
        $data->email      = $user->email;
        $data->link       = route('home');
        try{
            $send = Mail::to($data->email)->send(new PasswordResetNotification($data));
        }catch(\Exception $e){}

        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])):
            return response()->json(['success' => true], 200);
        endif;

        return response()->json(['success' => true], 200);
    }
}
