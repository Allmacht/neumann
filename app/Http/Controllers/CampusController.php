<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Campus;
use App\User;

class CampusController extends Controller
{
    public function index(Request $request)
    {
        if($request->ajax()){
            $campuses = Campus::select('id','code','name','status')->get();
            return response()->json(['success' => true,'data' => $campuses],200);
        }
        return view('Campuses.index');
    }

    public function create(Request $request)
    {
        if($request->ajax()){
            $users = User::role(['super-admin','Administrador'])->select('id','names')->whereStatus(true)->get();
            return response()->json($users);
        }
        return view('Campuses.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'campus.unique_id'       => 'required|unique:campuses,code',
            'campus.name'            => 'required|unique:campuses,name',
            'campus.user_id'         => 'required|numeric|exists:users,id',
            'campus.street'          => 'required',
            'campus.state'           => 'required',
            'campus.municipality'    => 'required',
            'campus.external_number' => 'required|numeric|min:0',
            'campus.zipcode'         => 'required|numeric|digits:5|min:0',
            'campus.level'           => 'required',
            'campus.shift'           => 'required',
            'campus.type'            => 'required'
        ],[
            'campus.unique_id.required'       => 'El Código único es requerido',
            'campus.unique_id.unique'         => 'El código único ya está en uso',
            'campus.name.required'            => 'El nombre del plantel es requerido',
            'campus.name.unique'              => 'El nombre ingresado ya está en uso',
            'campus.user_id.required'         => 'El Administrador es requerido',
            'campus.user_id.numeric'          => 'El Administrador seleccionado no es válido',
            'campus.user_id.exists'           => 'El Administrador seleccionado no es válido',
            'campus.street.required'          => 'La calle es requerida',
            'campus.state.required'           => 'El estado es requerido',
            'campus.municipality.required'    => 'El municipio es requerido',
            'campus.external_number.required' => 'El número exterior es requerido',
            'campus.external_number.numeric'  => 'El número exterior ingresado no es válido',
            'campus.external_number.min'      => 'El número exterior ingresado no es válido',
            'campus.zipcode.required'         => 'El código postal es requerido',
            'campus.zipcode.numeric'          => 'El código postal ingresado no es válido',
            'campus.zipcode.digits'           => 'El código postal debe contener 5 dígitos',
            'campus.zipcode.min'              => 'El código postal ingresado no es válido',
            'campus.level.required'           => 'El nivel es requerido',
            'campus.shift.required'           => 'El turno es requerido',
            'campus.type.required'            => 'El tipo de plantel es requerido'
        ]);

        if($validator->fails())
            return response()->json(['errors' => $validator->errors()],200);

        $campus = new Campus();
        $campus->code            = $request->campus['unique_id'];
        $campus->name            = $request->campus['name'];
        $campus->street          = $request->campus['street'];
        $campus->colony          = $request->campus['colony'];
        $campus->state           = $request->campus['state'];
        $campus->municipality    = $request->campus['municipality'];
        $campus->external_number = $request->campus['external_number'];
        $campus->internal_number = $request->campus['internal_number'];
        $campus->zipcode         = $request->campus['zipcode'];
        $campus->level           = $request->campus['level'];
        $campus->shift           = $request->campus['shift'];
        $campus->type            = $request->campus['type'];
        $campus->user_id         = $request->campus['user_id'];

        $campus->save();

        return response()->json(['success' => true],200);
    }

    public function validate_campus(Request $request)
{
        if($request->ajax()):
            $campus = Campus::whereCode($request->campus_code)->first();
            if(is_null($campus))
                return response()->json(['success' => false],200);
            return response()->json(['success' => true],200);
        endif;
    }
}
