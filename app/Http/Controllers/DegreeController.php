<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Degrees;
use App\User;

class DegreeController extends Controller
{
    public function index(Request $request){ 
        if($request->ajax()){
            $degree = Degrees::select('name','RVOE','semesters','status')->get();
            return response()->json(['success' => true,'data' => $degree],200);
        }
        return view('Degrees.index');
    }
    
    public function create(Request $request){
        return view('Degrees.create');
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(),[
            // 'degree.unique_id'          =>       'required|unique:degrees,code',
            'degree.name'                   =>          'required|unique:degrees,name',
            'degree.RVOE'                   =>          'required|size:7|unique:degrees,RVOE',
            'degree.semesters'              =>          'required|numeric|min:1',
            'degree.description'            =>          'required',
            'degree.dicipline'              =>          'required'
        ],[
            'degree.name.required'          =>          'El nombre de la carrera es requerido',
            'degree.RVOE.required'          =>          'El RVOE es requerido',
            'degree.RVOE.unique'            =>          'El RVOE ya esta en uso',
            'degree.RVOE.size'              =>          'El RVOE tiene que tener 7 digitos',
            'degree.semesters.required'     =>          'El campo semestres esta vacío',
            'degree.semesters.min'          =>          'El valor minimo de semestres es 1',
            'degree.description.required'   =>          'Se require de una minima descripción',
            'degree.dicipline.required'     =>          'Elegir una opcion en el campo diciplinas'
        ]);
        
        if($validator->fails())
            return response()->json(['errors' => $validator->errors()],200);

        $degree= new Degrees();
        // $degree->code               =$request->degree['unique_id'];
        $degree->name            =          $request->degree['name'];
        $degree->RVOE            =          $request->degree['RVOE'];
        $degree->semesters       =          $request->degree['semesters'];
        $degree->description     =          $request->degree['description'];
        // $degree->dicipline       =          $request->degree['dicipline'];

        $degree->save();

        return  response()->json(['success' => true],200);
    }
    
    public function validate_degrees(Request $request){
        if($request->ajax()):
            $degrees = Degrees::whereCode($request->degrees_code)->first();
            if(is_null($degrees))
            return response()->json(['success' => false],200);
            return response()->json(['sucess' => false],200);
        endif;
    } 
}  