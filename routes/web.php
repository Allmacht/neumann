<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true, 'reset' => false]);

Route::get('/home', 'HomeController@index')->middleware('verified')->name('home');
Route::post('campuses/validate_campus','CampusController@validate_campus')->name('campuses.validate_campus');
Route::post('/users/guest/store','UserController@guest_store')->name('users.guest_store');
Route::get('/email/validate/{user}','UserController@validate_email')->name('email.validate');
Route::post('/password/reset/email', 'UserController@email_reset')->name('email.reset');
Route::get('/password/reset/validate/{user}','UserController@password_reset')->name('password.reset');
Route::post('/password/reset/update', 'UserController@password_update')->name('password.update');

//Email verification routes
Route::middleware('auth')->prefix('email')->name('email.')->group(function(){
    Route::get('/resend_email','UserController@resend_email')->name('resend');
});

//User routes
Route::middleware(['auth'])->prefix('users')->name('users.')->group(function(){
    Route::get('getUser','UserController@getUser')->name('getUser');
});

//Campus routes
Route::middleware(['auth','role:super-admin|Administrador','verified'])->prefix('campuses')->name('campuses.')->group(function(){
    Route::get('/','CampusController@index')->name('index');
    Route::get('/create','CampusController@create')->name('create');

    Route::post('/store','CampusController@store')->name('store');
});

//Degrees routes
Route::middleware(['auth','role:super-admin|Administrador|Coordinador|Docente','verified'])->prefix('degrees')->name('degrees.')->group(function(){
    Route::get('/','DegreeController@index')->name('index');
    Route::get('/create','DegreeController@create')->name('create');

    Route::post('/store','DegreeController@store')->name('store');
    // Route::post('degrees/validate_degree','DegreeController@validate_degree')->name('degrees.validate_degree');

    Route::get('user/{id}', 'UserController@show');
});
