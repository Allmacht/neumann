@extends('layouts.app')
@section('styles')
    <link rel="stylesheet" href="{{ asset('css/layouts/index.css') }}">
@endsection
@section('content')
    <degrees-index></degrees-index>
@endsection
