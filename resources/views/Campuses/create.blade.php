@extends('layouts.app')
@section('styles')
    <link rel="stylesheet" href="{{asset('css/layouts/create.css')}}">
@endsection
@section('content')
        <campuses-create></campuses-create>
@endsection
