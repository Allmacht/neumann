<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body style="
    @import url('https://fonts.googleapis.com/css?family=Nunito&display=swap');
    font-family: 'Nunito', sans-serif !important;
    ">
    <div style="
        text-align: center !important;
        margin-bottom: 20px;
        ">
        <img src="https://iili.io/JK5yqQ.png" width="450px">
    </div>

    <div style="
        padding-top: 100px;
        padding-bottom: 100px;
        background: #f7f7f7;
        box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.15) !important;
        text-align: center !important;
        ">
        <div style="
            font-size: 19px;
            ">
            <h5><strong>Hola, {{$data->user_name}}.</strong></h5>
            <h5>Su contraseña se ha restablecido exitosamente, si no ha sido usted por favor contacte al Administrador.</h5>
        </div>
        <a href="{{$data->link}}" style="
                display: inline-block;
                font-weight: 400;
                text-align: center;
                vertical-align: middle;
                cursor: pointer;
                border: 1px solid transparent;
                color: #fff;
                background-color: #6cb2eb;
                border-color: #6cb2eb;
                padding: 0.5rem 1rem;
                font-size: 1.25rem;
                line-height: 1.5;
                border-radius: 0.3rem;
                text-decoration: none !important;
                margin-bottom: 100px;
            ">
            Neumann
        </a><br>
        <small>Si tienes problemas para hacer clic al botón, has clic en el siguiente enlace {{$data->link}} </small>
    </div>
    <div style="text-align: center !important;
                color: #fff !important;
                background-color: rgba(59, 127, 223, 0.5) !important;
                padding-top: 1rem !important;
                padding-bottom: 1rem !important;">
        <h4>{{__('© Neumann '.date('Y').". All rights reserved")}}</h4>
    </div>
</body>
</html>
