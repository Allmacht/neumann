@extends('layouts.app')

@section('content')
    <password-reset token="{{ $token }}"></password-reset>
@endsection
